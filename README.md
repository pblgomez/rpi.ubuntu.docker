This are the instructions to make a Ubuntu Server on a Raspberry Pi with docker and docker-compose

- Download the Ubuntu [preinstalled server image for Raspberry Pi 3B/3B+](http://cdimage.ubuntu.com/releases/18.04.2/release/ubuntu-18.04.2-preinstalled-server-arm64+raspi3.img.xz) or [preinstalled server image for RPi 2Raspberry Pi 2](http://cdimage.ubuntu.com/ubuntu/releases/18.04/release/ubuntu-18.04.2-preinstalled-server-armhf+raspi2.img.xz)

- Install the image to a SD (be careful!)
```sh
xzcat ubuntu.img.xz | sudo dd bs=4M of=/dev/mmcblk0
```

- To [change the bootloader](https://wiki.ubuntu.com/ARM/RaspberryPi#Change_the_bootloader) before 1st boot !
 
  Open a terminal in the system-boot partition
```sh
# Copy from the writable to the boot partition
cp -Rvu ../writable/lib/firmware/4.15.0-1031-raspi2/device-tree/overlays/ .

# Check what version of Pi and copy the file
cp -Rvu ../writable/lib/firmware/4.15.0-1031-raspi2/device-tree/broadcom/bcm2710-rpi-3-b-plus.dtb  .

# On the boot partition
sed -i 's/kernel=kernel8.bin/kernel=vmlinuz\ninitramfs initrd.img followkernel/' config.txt

sed -i 's/device/#device/' config.txt
```

- (Optional) To connect via wireless create this file on writable partition:
```
# /etc/netplan/config.yaml
network:
  version: 2
  renderer: networkd
  wifis:
    wlan0:
      dhcp4: yes
      dhcp6: yes
      nameservers:
        addresses: [1.0.0.1, 8.8.8.8]
      access-points:
        "NetworkName":
          password: "NetworkPassword"
```

- First boot:
  - Change password 
  ```sh
    sudo passwd $USER
  ```
  - Clone the repo and execute setup.sh
  ```sh
  git clone https://gitlab.com/pblgomez/rpi.ubuntu.docker.git
  cd rpi.ubuntu.docker && ./setup.sh
  ```