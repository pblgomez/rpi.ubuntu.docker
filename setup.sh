#!/usr/bin/env bash
set -e

#Assign existing hostname to $hostn
hostn=$(cat /etc/hostname)
#Display existing hostname
echo "Existing hostname is $hostn"
#Ask for new hostname $newhost
echo "Enter new hostname: "
read newhost
#change hostname in /etc/hosts & /etc/hostname
sudo sed -i "s/$hostn/$newhost/g" /etc/hosts
sudo sed -i "s/$hostn/$newhost/g" /etc/hostname
#display new hostname
echo "Your new hostname is $newhost"


echo "Reserve only 16MB for GPU"
# Check if this line exist
grep -q gpu_mem=16 /boot/firmware/config.txt || sudo sh -c "echo "gpu_mem=16" >> /boot/firmware/config.txt"

echo "Updating system"
sudo apt update
sudo apt full-upgrade -y

echo "Removing old docker just in case"
sudo apt-get remove -y docker docker-engine docker.io

echo "Installing docker CE"
sudo apt install docker.io -y

echo "Enabling docker"
sudo systemctl enable docker
echo "Starting docker"
sudo systemctl start docker
echo "Adding user to docker"
sudo usermod -aG docker $USER
newgrp docker << BLEBLE
newgrp $USER
BLEBLE

echo "Install docker-compose"
sudo apt-get install docker-compose -y

echo "Enabling docker-compose at boot"
sudo cp docker-compose-opt.service /etc/systemd/system/docker-compose-opt.service
sudo sed -i "s|<USER>|$USER|" /etc/systemd/system/docker-compose-opt.service
dockerpath=$(whereis docker-compose | cut -d ' ' -f2)
sudo sed -i "s|<DOCKERCOMPOSEPATH>|$dockerpath|" /etc/systemd/system/docker-compose-opt.service
sudo systemctl daemon-reload
sudo systemctl enable docker-compose-opt.service

# backup of docker-compose if exist
echo "Backing up docker-compose.yaml if exist"
if [ -f $HOME/docker-compose.yaml ]; then
  i=0
  while [[ -e $HOME/docker-compose-$i.yaml ]] ; do
    i=$((i+1))
  done
  name=docker-compose-$i.yaml
  cp $HOME/docker-compose.yaml $HOME/$name
fi
cp docker-compose.yaml $HOME/docker-compose.yaml

echo "Copying example of docker-compose.yaml"
cp docker-compose.yaml $HOME/docker-compose.yaml
while true; do
    read -p "Do you wish to clone docker-compose examples? [y/n]" yn
    case $yn in
        [Yy]* ) git clone https://gitlab.com/pblgomez/rpi.dockers.git $HOME/rpi.dockers ; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer y or n.";;
    esac
done
